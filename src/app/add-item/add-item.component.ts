import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormBuilder, FormGroup } from '@angular/forms';
import {DataServiceService} from '../services/data-service.service';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
// import { MyTestApp } from './my-test-app';
import {OwlDateTimeModule, OwlNativeDateTimeModule} from 'ng-pick-datetime';

@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.component.html',
  styleUrls: ['./add-item.component.css']
})
@NgModule({
  imports: [
    BrowserModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
  // ],
  // declarations: [ MyTestApp ],
  // bootstrap:    [ MyTestApp ]
  ]
})
export class AddItemComponent implements OnInit {

  displayBookForm = true;
  displayDVDForm = false;

  isbnNumber = new FormControl('', [Validators.required]);
  title = new FormControl('', [Validators.required]);
  books: FormGroup;
  dvd = {} as any;
  book = {} as any;

  constructor(public fb: FormBuilder, private service: DataServiceService) {
    this.books = fb.group({
      totalNumberOfPages: [1, Validators.min(1)],
    });
  }

  ngOnInit() {
  }

  getErrorISBNMessage() {
    return this.isbnNumber.hasError('required') ? 'You must enter a ISBN Number' : '';
  }

  getErrorNameMessage() {
    return this.title.hasError('required') ? 'You must enter a Title' : '';
  }

  displayBookFormMethod() {
    this.displayDVDForm = false;
    this.displayBookForm = true;
  }

  displayDVDFormMethod() {
    this.displayBookForm = false;
    this.displayDVDForm = true;
  }

  addBook(book) {
    book.totalNumberOfPages = this.books.value.totalNumberOfPages;
    // console.log(book);
    this.service.postRequest('addbook', book).subscribe(data => {
      console.log('New book list: ');
      console.log(data);
    });
  }

  cancelBook() {
    this.book = {};
    this.books = this.fb.group({
      noOfPages: [1, Validators.min(1)],
    });
  }

  addDVD(dvd) {
    this.service.postRequest('adddvd', dvd).subscribe(data => {
      console.log('New DVD List : ');
      console.log(data);
    });

    console.log(dvd);
  }

  cancelDVD() {
    this.dvd = {};
  }

}

// {
//   "publisher":"abc",
//   "totalNumberOfPages":100,
//   "allBooksInTheLibrary":null,
//   "MAX_BOOK_COUNT":10,
//   "currentBookCount":5,
//   "authors":null,
//   "remainingBookCount":5,
//   "isbnNumber":"abc",
//   "title":"abc",
//   "publicationDate":"abc",
//   "dateBorrowed":"abc",
//   "currentReader":"abc",
//   "allItemsInTheLibrary":null,
//   "MAX_ITEM_COUNT":6,
//   "currentItemCount":5,
//   "remainingItemCount":1
// }
