import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataServiceService {

  serverUrl = 'http://localhost:9000/';

  constructor(private http: HttpClient) { }

  getRequest(endpoint): Observable<any> {
    const ep = this.serverUrl + endpoint;
    console.log(ep);
    return this.http.get(ep);
  }

  postRequest(endpoint, data): Observable<any> {
    const ep = this.serverUrl + endpoint;
    const json = JSON.stringify(data);
    console.log(json);
    return this.http.post(ep, data);
    // return null;
  }
}
